# Install

Change file system to xfs.

# Config

## VLAN

Add stanza in network config on proxmox node:

```bash
nano /etc/network/interfaces`
```

replace `{{x}}` with vlan ID and `x` with subnet address.

```bash
auto vmbr0.{{x}}
iface vmbr0.{{x}} inet static
        address 10.0.x.2/24
```

Add NIC to created machine with vlan ID `{{x}}`.

## USB passthrough

Hardware tab of machine, add device `Use USB Vendor/Device ID`
and choose Device from list.

Command which can be usefull `lsusb`

device: memory stick Cruzer Blade USB 2.0

lsusb output: Bus 002 Device 004: ID 0781:5567 SanDisk Corp. Cruzer Blade 

ID xxxx:xxxx should be visible in list during adding passthrough device.

# Debug

##  command 'apt-get update' failed: exit code 100

[proxmox docs - repositories](https://forum.proxmox.com/threads/command-apt-get-update-failed-exit-code-100.42387/)

No valid proxmox subscription, comment out repo:

```bash
nano /etc/apt/sources.list.d/pve-enterprise.list
```
